import React from "react";
import BootstrapTable from "react-bootstrap-table-next";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import cellEditFactory from 'react-bootstrap-table2-editor';

export default class TableCell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cellEditProp: {
                mode: 'click',
                blurToSave: true,
                cellEdit: true
            },
            rowArr: []
        };
    };

    getRandomLength = (min, max) => {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    getApplicationArr = (min, max, rowIndex) => {
        const { rowArr } = this.state;
        console.log("currArr===> ", rowIndex, rowArr[rowIndex]);
        if (!rowArr[rowIndex]) {
            rowArr[rowIndex] = [...Array(this.getRandomLength(min, max)).keys()].map(p => {
                return {
                    id: (rowIndex + 1) + '' + p + this.getRandomLength(1000, 10000),
                    name: "Application name " + (p + 1),
                    price: "$" + (p + 1)
                };
            });
            return rowArr[rowIndex];
        } else {
            return rowArr[rowIndex];
        }
    };

    getApplicationColumnsArr = (rowIndex) => {
        console.log("rowIndex -> ", rowIndex);
        return [
            {
                dataField: "id",
                text: "Application ID"
            },
            {
                dataField: "name",
                text: "e Application Name",
                sort: true
            },
            {
                dataField: "price",
                text: "Application Price",
                sort: true
            }
        ];
    };

    getVendorColumnsArr = (rowIndex) => {
        console.log("rowIndex -> ", rowIndex);
        return [
            {
                dataField: "id",
                text: "Vendor ID",
                sort: true
            },
            {
                dataField: "name",
                text: "Vendor Name",
                sort: true
            }
        ];
    };

    getVendorArr = () => {
        return [...Array(this.getRandomLength(2, 10)).keys()].map(p => {
            return {
                id: (p + 1),
                name: "Vendor name " + (p + 1)
            };
        });
    };

    render() {
        const { cellEditProp } = this.state;
        const expandRow = {
            renderer: (row, rowIndex) => (
                <div>
                    <BootstrapTable
                        keyField="id"
                        data={this.getApplicationArr(2, 10, rowIndex)}
                        columns={this.getApplicationColumnsArr(rowIndex)}
                        cellEdit={cellEditFactory(cellEditProp)}                    >
                    </BootstrapTable>
                </div>
            ),
            showExpandColumn: true
        };
        return (
            <BootstrapTable
                keyField="id"
                data={this.getVendorArr()}
                columns={this.getVendorColumnsArr()}
                expandRow={expandRow}
            >
            </BootstrapTable>
        );
    };

}